import { sp } from '@pnp/sp/presets/all';
import { IListItem } from '../types/IListItem';
import { WebPartContext } from '@microsoft/sp-webpart-base';

export class DocumentListService {
	constructor(context: WebPartContext) {
		sp.setup({
			spfxContext: context
		});
	}
	public async getHecDocumentsList(listName: string): Promise<IListItem[]> {
		const result: IListItem[] = [];
		return new Promise<IListItem[]>(async (resolve, reject) => {
			sp.web.lists
				.getByTitle(listName)
				.items.filter("ContentType eq 'HECReferenz'")
				.expand('File')
				.getAll(1)
				.then((items) => {
					console.log(items);
					items.forEach((item) => {
						result.push({ title: item.File.Name || item.Title });
					});
					resolve(result);
				})
				.catch((reason) => {
					reject(reason);
				}); // assim se cria um excessao no TS.
		});
	}
}
