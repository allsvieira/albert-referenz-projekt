import { sp } from "@pnp/sp";
import "@pnp/sp/forms";
import "@pnp/sp/lists";
import { Webs, IWebs } from "@pnp/sp/webs";
import { getGUID } from "@pnp/common";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";
export class hecRefenzController
{
    public static async getData () :  Promise<any> {
        sp.setup({ sp: { baseUrl: "https://teamneusta.sharepoint.com/sites/HEC-Dokumente/_api/web" }, });

        
        const allItems: any[] = await 
        sp.web.lists.getByTitle("Dokumente").
        items
        .select("File")
        .expand("File")
        .filter("ContentType eq 'HECReferenz'")
        .getAll();//.then((response)=>) // in der anonym function konnten wir auch 
        
        
        return allItems;
    }
}