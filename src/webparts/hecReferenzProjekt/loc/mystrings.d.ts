declare interface IHecReferenzProjektWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'HecReferenzProjektWebPartStrings' {
  const strings: IHecReferenzProjektWebPartStrings;
  export = strings;
}
