import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import { IPropertyPaneConfiguration, PropertyPaneTextField } from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import * as strings from 'HecReferenzProjektWebPartStrings';
import HecReferenzProjekt from './components/HecReferenzProjekt';
import { IHecReferenzProjektProps } from './components/IHecReferenzProjektProps';
import { sp } from '@pnp/sp/presets/all';

export interface IHecReferenzProjektWebPartProps {
	listName: string;
}

export default class HecReferenzProjektWebPart extends BaseClientSideWebPart<IHecReferenzProjektWebPartProps> {
	public render(): void {
		const { listName } = this.properties;
		const { context } = this;

		const element: React.ReactElement<IHecReferenzProjektProps> = React.createElement(HecReferenzProjekt, {
			context,
			listName
		});

		ReactDom.render(element, this.domElement);
	}

	protected onDispose(): void {
		ReactDom.unmountComponentAtNode(this.domElement);
	}

	protected get dataVersion(): Version {
		return Version.parse('1.0');
	}

	protected onInit(): Promise<void> {
		return super.onInit().then((_) => {
			sp.setup({
				spfxContext: this.context,
			});
		});
	}
	protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
		return {
			pages: [
				{
					header: {
						description: strings.PropertyPaneDescription
					},
					groups: [
						{
							groupName: strings.BasicGroupName,
							groupFields: [
								PropertyPaneTextField('listName', {
									label: strings.DescriptionFieldLabel
								})
							]
						}
					]
				}
			]
		};
	}
}
