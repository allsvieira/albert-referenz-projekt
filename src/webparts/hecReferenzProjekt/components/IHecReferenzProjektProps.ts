import { WebPartContext } from '@microsoft/sp-webpart-base';

export interface IHecReferenzProjektProps {
	context: WebPartContext;
	listName: string;
}
