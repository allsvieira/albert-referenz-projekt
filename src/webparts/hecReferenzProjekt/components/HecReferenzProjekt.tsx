import * as React from 'react';
import { IHecReferenzProjektProps } from './IHecReferenzProjektProps';
import { DetailsList } from 'office-ui-fabric-react';
import { DocumentListService } from '../../../Service/DocumentListService';
import { IListItem } from '../../../types/IListItem';

export interface IHecReferenzProjekState {
	listItems: Array<IListItem>;
}

export default class HecReferenzProjekt extends React.Component<IHecReferenzProjektProps, IHecReferenzProjekState> {
	private _service: DocumentListService = null;
	constructor(props: IHecReferenzProjektProps) {
		super(props);

		this._service = new DocumentListService(this.props.context);
		this.state = { listItems: [] };
	}

	public componentDidMount() {
		this._service
			.getHecDocumentsList(this.props.listName)
			.then((listItems: IListItem[]) => {
				console.log(listItems);
				this.setState({ listItems });
			})
			.catch((error) => {
				console.error(error);
			});
	}

	public render(): React.ReactElement<IHecReferenzProjektProps> {
		return <DetailsList items={this.state.listItems} />;
	}
}
